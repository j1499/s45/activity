import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register() {

    const {user} = useContext(UserContext)
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const [isActive, setIsActive] = useState(false)  
    
    const navigate = useNavigate();

    // console.log(email);
    // console.log(password);
    // console.log(verifyPassword);

    function registerUser(e) {
        e.preventDefault()

        fetch('http://localhost:4000/users/checkEmail', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email // From email state
            })
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            if (data) {
                Swal.fire({
                    title: "Duplicate email found!", 
                    icon: "error",
                    text: "Please try another email address"
                })
                
            } else {
                console.log(data);
                fetch('http://localhost:4000/users/register', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        Swal.fire({
                            title: "Registration Successful!", 
                            icon: "success",
                            text: "You may now log in"
                        })

                        setEmail('')
                        setFirstName('')
                        setLastName('')
                        setMobileNo('')
                        setPassword('')

                        navigate("/login")
                    
                    } else {
                        Swal.fire({
                            title: "Registration failed!",
                            icon: "error", 
                            text: "Please try again."
                        })
                    }
                })
            }
        })
    }

    // Discussion
    useEffect(() => {

    if((firstName !== '' && 
      lastName !== '' && 
      mobileNo !== '' && 
      email !== '' && 
      password !== '') && 
      (mobileNo.length === 11) && (password === verifyPassword)) {
      
      setIsActive(true);

    } else {
      
      setIsActive(false);
    
    }

  }, [firstName, lastName, mobileNo, email, password, verifyPassword])

    return (
        (user.id !== null) ? 
      <Navigate to ="/courses" />
        :   
        <Form className="mt-3" onSubmit = {(e) => registerUser(e)}>
            <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                                type="text" 
                                placeholder="Enter first name"
                                value = {firstName}
                                onChange = {e => setFirstName(e.target.value)} 
                                required />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                                type="text" 
                                placeholder="Enter last name"
                                value = {lastName}
                                onChange = {e => setLastName(e.target.value)} 
                                required />
            </Form.Group>

            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value = {email}
                                onChange = {e => setEmail(e.target.value)} 
                                required />
                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                                type="text"
                                // minLength={11}
                                // maxLength={11}
                                // pattern="[0][9][0-9]{9}" 
                                placeholder="Enter mobile number"
                                value = {mobileNo}
                                onChange = {e => setMobileNo(e.target.value)} 
                                required />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value = {password}
                                onChange = {e => setPassword(e.target.value)} 
                                required />
            </Form.Group>

            <Form.Group className="mb-3" controlId="verifyPassword">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                                type="password" 
                                placeholder="Verify Password"
                                value = {verifyPassword}
                                onChange = {e => setVerifyPassword(e.target.value)} 
                                required />
            </Form.Group>

            {
                isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
            }
        </Form>
    )
}

// /*
//   Mini-Activity
//     If the user goes to /register route(Registration Page) when the user is already logged in, navigate/redirect to the courses page
  
// */