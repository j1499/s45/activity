import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout() {

	const {unsetUser, setUser} = useContext(UserContext);
	unsetUser();	

	// localStorage.clear() will allow us to clear the information in the local storage
	// localStorage.clear()
	

	// add a useEffect to run our setUser. This useEffect will have an empty dependency array.
	useEffect(() => {
		setUser({
			id: null
		})

	},[])

	return(

		<Navigate to="/login" />

		)

}