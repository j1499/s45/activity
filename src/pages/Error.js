import { Link } from 'react-router-dom';
import { Row, Col, Button } from 'react-bootstrap';

export default function Error(){

	return(
		<Row>
			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>Go back to the homepage.</p>
				<Button as={ Link } to="/" variant="primary">Homepage</Button>
			</Col>
		</Row>
		 		
		)
};