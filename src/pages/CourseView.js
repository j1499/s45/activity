import { useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Col, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CourseView() {

	const { user } = useContext(UserContext);

	// useNavigate returns function
	const navigate = useNavigate();

	// useParams() hook allows us to retrieve the courseId passed via the URL
	const { courseId } = useParams()
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	// Function for enrolling
	const enroll = (courseId) => {

		fetch("http://localhost:4000/users/enroll", {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {

			// console.log(data) 
			// boolean result from api
			if(data === true) {
				Swal.fire({
					title: "Successfully enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})

				// Navigate to /courses page when successfully enrolled
				navigate("/courses")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		
		})

	}


	// Fetch retrieve specific course
	useEffect(() => {

		// console.log(courseId)
		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {

			// console.log(data)
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		
		})

	}, [courseId])







	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{ span:6, offset:3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>Class Schedule:</Card.Subtitle>
							<Card.Text>8 AM - 5 PM</Card.Text>
							{
								(user.id !== null) ?
									<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
									:
									<Link className="btn btn-danger" to="/login">Login to Enroll</Link>

							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)

}