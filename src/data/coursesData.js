
const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Unde a at animi optio architecto eum eaque facilis, dolores alias harum vel nesciunt libero, in commodi id ullam soluta. Inventore, explicabo.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Unde a at animi optio architecto eum eaque facilis, dolores alias harum vel nesciunt libero, in commodi id ullam soluta. Inventore, explicabo.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Unde a at animi optio architecto eum eaque facilis, dolores alias harum vel nesciunt libero, in commodi id ullam soluta. Inventore, explicabo.",
		price: 55000,
		onOffer: true
	}

]

export default coursesData;