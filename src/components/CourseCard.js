// import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function CourseCard({courseProp}) {

	// console.log(props)
	// console.log(typeof props)
	// console.log(props.courseProp.name)
	// console.log(courseProp)
	const { name, description, price, _id } = courseProp;
	// console.log(courseProp)

	/*
		Syntax for useState
			const = [getter, setter] = useState(initialGetterValue)
	*/

	// const [count, setCount] = useState(0)

	// function enroll () {
		
	// 	// console.log(count)
	// 	if (count === 30){
	// 		alert(`No more seats!`)
	// 	} else {
	// 		setCount(count + 1)
	// 		console.log('Enrollees:' + count)
	// 	}
		
	// }
		// const [count, setCount] = useState(0)
		// const [seats, setSeats] = useState(30)

		// function enroll () {
		// 	setCount(count + 1);
		// 	console.log('Enrollees: ' + count);
		// 	setSeats(seats - 1);
		// 	console.log('Seats: ' + seats)
		// }

		// useEffect(() => {

		// 	if (seats === 0) {
		// 		alert('No more seats available.');
		// 	}
		// }, [seats]) 
		
	

	
	return (
		<Row>
			<Col>
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<h6>Description:</h6>
						<p>{description}</p>
						<h6>Price:</h6>
						<p>Php {price}</p>
						{/*<p>Enrollees: {count}</p>
						<p>Seats: {seats}</p>
						<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
						<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}