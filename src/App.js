
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
//Fragment - if ever has multiple components error will not occur
// import {Fragment} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
//Import Banner
// import Banner from './components/Banner';
//Import Highlights
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView'
import Home from './pages/Home'; 
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Error from './pages/Error';
import './App.css';

function App() {

  // Initial value is null from the useContext
  const [user, setUser] = useState({
     // email: localStorage.getItem('email')
     id: null,
     isAdmin: null
  })

// Function to clear the localStorage for logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Fetch the localStorage
  useEffect( () => {
    // console.log(user, "User from app.js")
    // console.log(localStorage)

    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== 'undefined') {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })

      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })

  }, [])

  return (
    // UserProvider from UserContext file
    <UserProvider value= {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>} />
              <Route exact path="/courses" element={<Courses/>} />
              <Route exact path="/courses/:courseId" element={<CourseView/>} />
              <Route exact path="/login" element={<Login/>} />
              <Route exact path="/logout" element={<Logout/>} />
              <Route exact path="/register" element={<Register/>} />
              <Route exact path="*" element={<Error />} />
            </Routes>
          </Container>   
      </Router>
    </UserProvider>  
   
  );
}

export default App;
